INSERT INTO users (user_first_name, user_last_name, user_address, user_email, user_phone)
VALUES ('Никита', 'Целищев', 'Киров, улица Октябрьский проспект, дом 117', 'nikita.tselischev@example.com',
        '+79000000000'),
       ('Алексей', 'Иванов', 'Москва, улица Тверская, дом 10', 'alex.ivanov@example.com', '+79001234567'),
       ('Екатерина', 'Смирнова', 'Санкт-Петербург, улица Невский проспект, дом 20', 'ekaterina.smith@example.com',
        '+79009876543'),
       ('Дмитрий', 'Петров', 'Екатеринбург, улица Ленина, дом 5', 'dmitry.petrov@example.com', '+79007654321'),
       ('Ольга', 'Козлова', 'Новосибирск, улица Красный проспект, дом 15', 'olga.kozlova@example.com', '+79003456789'),
       ('Иван', 'Соколов', 'Казань, улица Баумана, дом 8', 'ivan.sokolov@example.com', '+79006543210'),
       ('Наталья', 'Морозова', 'Самара, улица Ленинская, дом 3', 'nata.morozova@example.com', '+79002345678'),
       ('Сергей', 'Павлов', 'Ростов-на-Дону, улица Большая Садовая, дом 12', 'sergey.pavlov@example.com',
        '+79008765432'),
       ('Анна', 'Федорова', 'Уфа, улица Комсомольская, дом 7', 'ann.fedorova@example.com', '+79004567890'),
       ('Юлия', 'Васильева', 'Омск, улица Красная, дом 30', 'yulia.vasilyeva@example.com', '+79007890123');

INSERT INTO products (product_name, product_cost, product_description)
VALUES ('Ноутбук', 90000.00, '16-дюймовый, 16 ГБ ОЗУ, 1 ТБ SSD'),
       ('Смартфон', 59990.99, '6,5-дюймовый, 128 ГБ, поддерживает 5G'),
       ('Наушники', 7999.99, 'Беспроводные, шумоподавление, Bluetooth 5-ое поколение'),
       ('Камера', 69900.99, '24 МП, запись видео 4K, сменный объектив'),
       ('Принтер', 21099.99, 'Беспроводной, цветной, всё в одном'),
       ('Монитор', 24000.00, '27-дюймовый, частота обновления 144 Гц, регулируемая подставка'),
       ('Клавиатура', 15000.00, 'Механическая, подсветка RGB, проводная'),
       ('Беспроводная мышь', 8499.99, 'Беспроводная, эргономичная, программируемые кнопки'),
       ('Планшет', 34990.99, '10-дюймовый, 64 ГБ, в комплекте стилус'),
       ('Смарт часы', 7500.00, 'Фитнес трекинг, замер сердечного ритма, водонепроницаемые');

INSERT INTO products(product_name, product_cost, product_description)
VALUES ('Кроссовки Nike', 18999.90, 'Nike Air Force 1, 44 размер, мужские, белые'),
       ('Кроссовки Nike', 17999.99, ' Nike LeBron 7, 43 размер, мужские, для баскетола'),
       ('Кроссовки Adidas', 16999.90, 'Adidas Streetball, 45 размер, мужские, черные'),
       ('Кроссовки Adidas', 19999.90, 'Adidas OZWEEGO, 44 размер, мужские, черные'),
       ('Кроссовки Nike', 29999.00, 'Nike Zoom Fly, 37 размер, женские, для бега'),
       ('Кроссовки Nike', 125000.00, 'Nike Grateful Dead x Dunk Low SB Green Bear, 44 размер, мужские, зеленые');

INSERT INTO products(product_name, product_cost, product_description)
VALUES ('Кроссовки Nike', 18999.90, 'Nike Air Force 1, 44 размер, мужские, белые'),
       ('Кроссовки Nike', 17999.99, ' Nike LeBron 7, 43 размер, мужские, для баскетола'),
       ('Кроссовки Adidas', 16999.90, 'Adidas Streetball, 45 размер, мужские, черные'),
       ('Кроссовки Adidas', 19999.90, 'Adidas OZWEEGO, 44 размер, мужские, черные'),
       ('Кроссовки Nike', 29999.00, 'Nike Zoom Fly, 37 размер, женские, для бега'),
       ('Кроссовки Nike', 125000.00, 'Nike Grateful Dead x Dunk Low SB Green Bear, 44 размер, мужские, зеленые');

INSERT INTO products (product_name, product_cost, product_description)
VALUES ('Плед', 1999.00, 'Мягкий плед из натурального хлопка, размер 150х200 см'),
       ('Одеяло', 2499.00, 'Утепленное одеяло, размер 200х220 см'),
       ('Подушка', 1299.00, 'Ортопедическая подушка с эффектом памяти, размер 50х70 см'),
       ('Постельное белье', 1800.00, 'Комплект постельного белья из сатина, на 2-спальную кровать'),
       ('Покрывало', 1499.99, 'Легкое покрывало, размер 200х240 см'),
       ('Шторы', 2999.00, 'Текстильные шторы с утяжелителями, летние, размер 140х250 см'),
       ('Ковер', 4999.00, 'Ковер с коротким ворсом, с антискользящим покрытием, размер 160х230 см'),
       ('Матрас', 7399.00, 'Ортопедический матрас памяти, жесткий, размер 160х200 см');

INSERT INTO products (product_name, product_cost, product_description)
VALUES ('Крем для лица', 2499.99, 'Увлажняющий крем с витамином Е и алоэ вера, 50 мл'),
       ('Шампунь', 149.99, 'Шампунь мужской, 500 мл'),
       ('Дезодорант', 299.00, 'Дезодорант-антиперспирант с ароматом лаванды, 50 мл'),
       ('Маска для волос', 999.00, 'Увлажняющая маска для сухих и поврежденных волос, 200 мл'),
       ('Парфюм', 3999.00, 'Женский парфюм с цветочными нотами, 50 мл'),
       ('Гель для душа', 249.00, 'Освежающий гель для душа с алоэ вера, 300 мл'),
       ('Зубная паста', 199.00, 'Отбеливающая зубная паста с мятой, 100 г'),
       ('Тушь для ресниц', 599.00, 'Удлиняющая тушь для ресниц с эффектом объема, черного цвета');

INSERT INTO products (product_name, product_cost, product_description)
VALUES ('Мед', 499.99, 'Натуральный цветочный мед, 500 г'),
       ('Кофе', 369.99, 'Арабика молотый, 250 г'),
       ('Чай', 179.99, 'Зеленый чай с жасмином, 100 г'),
       ('Хлеб', 39.00, 'Буханка хлеба, 400 г'),
       ('Молоко', 75.00, 'Молоко 2,5%, 1л'),
       ('Торт', 799.00, 'Домашний торт прага, вес 1 кг'),
       ('Консервированные фрукты', 199.99, 'Ассорти из ананасов, персиков и манго, 720 мл'),
       ('Сок', 299.00, 'Апельсиновый, свежевыжатый, 1л');

INSERT INTO categories (category_name)
VALUES ('Одежда и аксессуары'),
       ('Электроника и гаджеты'),
       ('Красота и здоровье'),
       ('Спорт и фитнес'),
       ('Детские товары'),
       ('Дом и сад'),
       ('Активный отдых'),
       ('Авто и мото'),
       ('Искусство и рукоделие'),
       ('Еда и напитки');

INSERT INTO inventory (product_id, product_count_remaining)
VALUES (1, 3),
       (2, 15),
       (3, 14),
       (4, 4),
       (5, 10),
       (6, 5),
       (7, 12),
       (8, 30),
       (9, 10),
       (10, 17),
       (11, 6),
       (12, 4),
       (13, 8),
       (14, 5),
       (15, 3),
       (16, 2),
       (17, 14),
       (18, 21),
       (19, 30),
       (20, 15),
       (21, 10),
       (22, 9),
       (23, 4),
       (24, 16),
       (25, 18),
       (26, 25),
       (27, 12),
       (28, 30),
       (29, 22),
       (30, 28),
       (31, 40),
       (32, 19),
       (33, 20),
       (34, 20),
       (35, 24),
       (36, 16),
       (37, 14),
       (38, 7),
       (39, 30),
       (40, 10);

INSERT INTO products_categories (product_id, category_id)
VALUES (1, 2),
       (2, 2),
       (3, 2),
       (4, 2),
       (5, 2),
       (6, 2),
       (7, 2),
       (8, 2),
       (9, 2),
       (10, 2),
       (10, 4),
       (11, 1),
       (11, 4),
       (12, 1),
       (12, 4),
       (13, 1),
       (13, 4),
       (14, 1),
       (14, 4),
       (15, 1),
       (15, 4),
       (16, 1),
       (16, 4),
       (17, 6),
       (18, 6),
       (19, 6),
       (20, 6),
       (21, 6),
       (22, 6),
       (23, 6),
       (24, 6),
       (25, 3),
       (26, 3),
       (27, 3),
       (28, 3),
       (29, 3),
       (30, 3),
       (31, 3),
       (32, 3),
       (33, 10),
       (34, 10),
       (35, 10),
       (36, 10),
       (37, 10),
       (38, 10),
       (39, 10),
       (40, 10);

INSERT INTO orders (order_date, order_status, user_id)
VALUES ('2023-01-10 10:30:00', 'Завершен', 1),
       ('2023-03-20 15:00:00', 'Отменен', 1),
       ('2023-02-15 16:45:00', 'Выполняется', 5),
       ('2023-04-25 09:00:00', 'Завершен', 3),
       ('2023-01-10 11:30:00', 'Завершен', 10),
       ('2023-03-20 12:00:00', 'Завершен', 7),
       ('2023-02-15 15:45:00', 'Завершен', 6),
       ('2023-04-25 20:00:00', 'Завершен', 2),
       ('2023-01-10 10:30:00', 'Завершен', 9),
       ('2023-03-20 19:00:00', 'Отменен', 8),
       ('2023-02-15 18:45:00', 'Выполняется', 8),
       ('2023-04-25 10:00:00', 'Завершен', 4);

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 1, 1, p.product_cost
FROM products p
WHERE p.product_id = 16;

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 1, 1, p.product_cost
FROM products p
WHERE p.product_id = 1;

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 2, 1, p.product_cost
FROM products p
WHERE p.product_id = 13;

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 3, 3, p.product_cost
FROM products p
WHERE p.product_id = 40;

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 4, 3, p.product_cost
FROM products p
WHERE p.product_id = 28;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 4, 1, p.product_cost
FROM products p
WHERE p.product_id = 25;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 4, 1, p.product_cost
FROM products p
WHERE p.product_id = 29;

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 5, 1, p.product_cost
FROM products p
WHERE p.product_id = 15;

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 6, 1, p.product_cost
FROM products p
WHERE p.product_id = 17;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 6, 1, p.product_cost
FROM products p
WHERE p.product_id = 18;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 6, 1, p.product_cost
FROM products p
WHERE p.product_id = 19;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 6, 1, p.product_cost
FROM products p
WHERE p.product_id = 20;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 6, 1, p.product_cost
FROM products p
WHERE p.product_id = 24;

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 7, 1, p.product_cost
FROM products p
WHERE p.product_id = 2;

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 7, 1, p.product_cost
FROM products p
WHERE p.product_id = 3;

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 8, 1, p.product_cost
FROM products p
WHERE p.product_id = 6;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 8, 1, p.product_cost
FROM products p
WHERE p.product_id = 7;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 8, 1, p.product_cost
FROM products p
WHERE p.product_id = 8;

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 9, 1, p.product_cost
FROM products p
WHERE p.product_id = 20;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 9, 1, p.product_cost
FROM products p
WHERE p.product_id = 32;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 9, 1, p.product_cost
FROM products p
WHERE p.product_id = 10;

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 10, 1, p.product_cost
FROM products p
WHERE p.product_id = 36;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 10, 1, p.product_cost
FROM products p
WHERE p.product_id = 37;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 10, 1, p.product_cost
FROM products p
WHERE p.product_id = 38;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 10, 2, p.product_cost
FROM products p
WHERE p.product_id = 39;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 10, 1, p.product_cost
FROM products p
WHERE p.product_id = 33;


INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 11, 1, p.product_cost
FROM products p
WHERE p.product_id = 4;

INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 12, 1, p.product_cost
FROM products p
WHERE p.product_id = 12;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 12, 1, p.product_cost
FROM products p
WHERE p.product_id = 26;
INSERT INTO order_details (product_id, order_id, product_count, product_cost)
SELECT p.product_id, 12, 1, p.product_cost
FROM products p
WHERE p.product_id = 10;

UPDATE orders AS o
SET order_total_cost = od.total_cost
FROM (SELECT order_id, SUM(product_cost * product_count) AS total_cost
      FROM order_details
      GROUP BY order_id) AS od
WHERE o.order_id = od.order_id;

UPDATE inventory AS i
SET product_count_remaining = product_count_remaining - od.product_count
FROM order_details AS od
JOIN orders AS o ON od.order_id = o.order_id
WHERE o.order_status IN ('Завершен', 'Выполнен')
AND i.product_id = od.product_id;