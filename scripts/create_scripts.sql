CREATE TABLE users
(
    user_id         BIGSERIAL PRIMARY KEY,
    user_first_name VARCHAR(255) NOT NULL,
    user_last_name  VARCHAR(255) NOT NULL,
    user_address    VARCHAR(255) NOT NULL,
    user_email      VARCHAR(255) NOT NULL,
    user_phone      VARCHAR(20)  NOT NULL
);

CREATE TABLE orders
(
    order_id         BIGSERIAL PRIMARY KEY,
    order_date       TIMESTAMP   NOT NULL,
    order_status     VARCHAR(50) NOT NULL,
    order_total_cost DECIMAL(10, 2),
    user_id          BIGINT,
    FOREIGN KEY (user_id) REFERENCES users (user_id)
);

CREATE TABLE products
(
    product_id          BIGSERIAL PRIMARY KEY,
    product_name        VARCHAR(255) NOT NULL,
    product_cost        DECIMAL(10, 2),
    product_description TEXT         NOT NULL
);

CREATE TABLE order_details
(
    product_id    BIGINT,
    order_id      BIGINT,
    product_count BIGINT NOT NULL,
    product_cost  DECIMAL(10, 2),
    FOREIGN KEY (order_id) REFERENCES orders (order_id),
    FOREIGN KEY (product_id) REFERENCES products (product_id)
);

CREATE TABLE categories
(
    category_id   BIGSERIAL PRIMARY KEY,
    category_name VARCHAR(255) NOT NULL
);

CREATE TABLE inventory
(
    product_id              BIGINT PRIMARY KEY REFERENCES products (product_id),
    product_count_remaining BIGINT NOT NULL
);

CREATE TABLE products_categories
(
    product_id  BIGINT,
    category_id BIGINT,
    primary key (product_id, category_id),
    FOREIGN KEY (product_id) REFERENCES products (product_id),
    FOREIGN KEY (category_id) REFERENCES categories (category_id)
);
